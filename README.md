# GCF_Web-Catcher

Version 1.0.0 - Local development
Version 1.1.0 - GCF Deploy
Version 1.2.0 - Credentials and Bucket save
Version 1.3.0 - Push to PubSub

Google Cloud Function - Web Catcher
Accepts web requests formatted to match Fuel Recovery 2 Web Services.